package com.test;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

import com.reger.saio.core.Server;

public class TestAioServer {

	public static void main(String[] args) throws InterruptedException {
		Server.<Object>newBulider().port(12345).completed((session, s) -> {
			ByteBuffer buffer = ByteBuffer.allocate(1024);
			session.receive(buffer, (size, at) -> {
				System.err.println("服务端成功接受 数据 size:" + size);
//				buffer.flip();
				session.send(buffer, (size1, at1) -> {
					System.err.println("服务端成功发送 数据 size:" + size1);
				}, null);
			}, null);

		}).failed((exc, s) -> {

		}).build().accept();
		TimeUnit.SECONDS.sleep(600);
	}

//	private final static ScriptEngine jse = new ScriptEngineManager().getEngineByName("JavaScript");
//	private static String eval(String body) {
//		// 将数据写回客户端
//		String relust;
//		try {
//			relust = jse.eval(body).toString();
//		} catch (ScriptException e) {
//			relust = "操作失败: " + e.getMessage();
//		}
//		return relust;
//	}
}
