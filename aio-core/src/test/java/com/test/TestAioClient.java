package com.test;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

import com.reger.saio.core.Client;

public class TestAioClient {
	public static void main(String[] args) throws InterruptedException {
		ByteBuffer buffer=ByteBuffer.allocate(1024);
		Client.newBulider().port(12345).build().connect()
		.receive(buffer, (size,at)->{
			buffer.flip();
			System.err.println("成功接受数据包大小 : "+size);
			System.err.println("成功接受数据包 : "+new String(buffer.array()));
		}, null)
		.send(ByteBuffer.wrap("23232423424234".getBytes()),  (size,at)->{
			System.err.println("成功发送数据包大小 : "+size);
		}, null);
		
		TimeUnit.SECONDS.sleep(600);
	}
}
