package com.reger.saio;

@FunctionalInterface
public interface FailedCallBack<T> {
	
	public void failed(Throwable exc, T attachment);
}
