package com.reger.saio;

@FunctionalInterface
public interface CompletedCallBack<C,T> {

	public void completed(C channel, T attachment);

}
